'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.query && req.query.token) {
    // save to session
    req.session.token = req.query.token;
    return res.status(302).redirect('/dashboard');
  } else {
    return res.render('home', {
      title: 'Homepage'
    });
  }
});

module.exports = router;
