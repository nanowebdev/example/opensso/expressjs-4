FROM node:16-alpine

WORKDIR /app/express-4

COPY package*.json ./

RUN npm install

COPY . ./

EXPOSE 8000