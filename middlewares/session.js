'use strict';

function checkSession(req, res, next) {
    const session = JSON.parse(JSON.stringify(req.session));
    if (session) {
      const token = session.token;
      if (token) {
        return next();
      }
    }
    return res.status(302).redirect('/');
}

module.exports = checkSession;