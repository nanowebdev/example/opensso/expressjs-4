'use strict';
var jwt = require('../lib/jwt')

function verifyJwt(req, res, next) {
    const session = JSON.parse(JSON.stringify(req.session));
    const token = session.token;
    try {
      jwt.verify(token, function(err, decode) {
        if(err) {
          return res.status(302).redirect('/logout');
        }
        return next();
      });
    } catch (e) {
      return res.status(302).redirect('/logout');
    }
}

module.exports = verifyJwt;