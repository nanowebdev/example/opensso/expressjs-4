'use strict';

var express = require('express');
var router = express.Router();
var checkSession = require('../middlewares/session');
var verifyJwt = require('../middlewares/jwt');
var jwt = require('../lib/jwt')

/* GET dashboard page. */
router.get('/', checkSession, verifyJwt, function(req, res, next) {
  var decoded = jwt.decode(req.session.token);
  res.render('dashboard', { title: 'Dashboard', jwt: decoded.payload });
});

module.exports = router;
