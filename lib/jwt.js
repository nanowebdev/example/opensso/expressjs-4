'use strict';

const fs = require('fs');
const jwt = require('jsonwebtoken');
const { join } = require('path')

// use 'utf8' to get string instead of byte array  (512 bit key)
const privateKEY = fs.readFileSync(join(__dirname,'..')+'/private.key', 'utf8');
const publicKEY = fs.readFileSync(join(__dirname,'..')+'/public.key', 'utf8');

const jwtOptions = {
  expiresIn: '8h',
  algorithm: 'RS256'
};

/**
 * Generate JWT
 * @param {object} payload
 * @param {object} $Options
 * @param {callback} callback
 * @return {callback}
 */
function sign (payload, callback) {
  jwt.sign(payload, privateKEY, jwtOptions, function (err, token) {
    if (err) return callback(err);
    return callback(null, token);
  });
}

/**
 * Verify JWT
 * @param {string} token
 * @param {callback} callback
 * @return {callback}
 */
function verify (token, callback) {
  jwt.verify(token, publicKEY, jwtOptions, function (err, decode) {
    if (err) return callback(err);
    return callback(null, decode);
  });
}

/**
 * Decode JWT
 * @param {string} token
 * @returns {object}
 */
function decode (token) {
  return jwt.decode(token, { complete: true });
}

module.exports = {
  sign,
  verify,
  decode
};
