'use strict';

var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  req.session.token = null;
  return res.status(302).redirect('/');
});

module.exports = router;
